<?php

namespace Coral\Core;

use Coral\CoralException;
use Coral\Tool\ConfTool;
use Coral\Tool\FileDirTool;
use Coral\Tool\UtilTool;
use Exception;
use Throwable;

class Logger
{
    const SUCCESS  = 0;
    const INFO     = 1;
    const WARNING  = 2;
    const ERROR    = 3;
    const CRITICAL = 4;
    const DEBUG    = 5;
    private ?int   $cutSize     = 0;
    private string $TraceID     = '';
    private bool   $isJsonPrint = false;
    private string $log         = '';
    private string $logDir      = '/Log';
    private string $logDateDir  = '';
    private string $fileName    = 'system.log';
    private bool   $output      = false;
    private string $lockFile    = 'composer.json';

    public function __construct()
    {

    }

    /**
     * @throws CoralException
     */
    public function SetInitModuleConfig($fileName, string $module = ''): Logger
    {
        $LogConfig    = ConfTool::GetConfig('Log');
        $this->output = $LogConfig['output'] ?? false;
        if (empty($module)) {
            $this->logDir     = CORAL_ROOT . DIRECTORY_SEPARATOR . $LogConfig['path'];
            $this->logDateDir = $this->logDir;
        } else {
            $this->logDir     = CORAL_ROOT . DIRECTORY_SEPARATOR . $LogConfig['path'] . '/' . $module;
            $this->logDateDir = UtilTool::NormalizedPath($this->logDir . DIRECTORY_SEPARATOR . date('y-m-d', time()));
        }


        $this->lockFile = CORAL_ROOT . DIRECTORY_SEPARATOR . $LogConfig['file_lock'];
        $this->filePathIsExist();
        if (empty($fileName)) {
            $this->fileName = $fileName . '.log';
        }
        $this->SetIsJsonPrint($LogConfig['json_print'] ?? false);
        $this->SetCutSize($LogConfig['cut_size'] ?? 0);
        return $this;
    }

    /**
     * @throws CoralException
     */
    public function SetInitFileConfig($fileName): Logger
    {
        $LogConfig        = ConfTool::GetConfig('Log');
        $this->output     = $LogConfig['output'] ?? false;
        $this->logDir     = CORAL_ROOT . DIRECTORY_SEPARATOR . $LogConfig['path'];
        $this->logDateDir = UtilTool::NormalizedPath($this->logDir);
        $this->lockFile   = CORAL_ROOT . DIRECTORY_SEPARATOR . $LogConfig['file_lock'];
        $this->filePathIsExist();
        $this->fileName = $fileName . '.log';
        $this->SetIsJsonPrint($LogConfig['json_print'] ?? false);
        $this->SetCutSize($LogConfig['cut_size'] ?? 0);
        return $this;
    }

    public function GetLogStr(): string
    {
        return $this->log;
    }

    public function SetTraceID(string $TraceID): Logger
    {
        $this->TraceID = $TraceID;
        return $this;
    }

    public function SetIsJsonPrint(bool $isJsonPrint): Logger
    {
        $this->isJsonPrint = $isJsonPrint;
        return $this;
    }

    public function SetCutSize(int $cutSize = 50 * 1024 * 1024): Logger
    {
        $this->cutSize = $cutSize;
        return $this;
    }

    public function PutContents()
    {
        $logFileName = UtilTool::NormalizedPath($this->logDateDir . DIRECTORY_SEPARATOR . $this->fileName);
        $log         = $this->log;
        if (file_exists($logFileName) && filesize($logFileName) > $this->cutSize && $this->cutSize != 0) {
            $goFileNamePath = UtilTool::NormalizedPath($this->logDateDir . DIRECTORY_SEPARATOR . $this->fileName);
            $toFileNamePath = UtilTool::NormalizedPath($this->logDateDir . DIRECTORY_SEPARATOR . date('ymdHis') . '_' . uniqid() . '_' . $this->fileName);
            FileDirTool::moveFile($goFileNamePath, $toFileNamePath, false);
        }
        file_put_contents($logFileName, $log, FILE_APPEND | LOCK_EX);
    }

    /**
     * @throws CoralException
     */
    public function filePathIsExist()
    {
        try {
            if (!empty($this->lockFile)) {
                if (!is_dir($this->logDateDir)) {
                    $file = fopen($this->lockFile, 'r');
                    if ($file) {
                        if (flock($file, LOCK_EX | LOCK_NB)) {
                            mkdir($this->logDateDir, 0777, true);
                            flock($file, LOCK_UN);
                        }
                    }
                    fclose($file);
                }
            } else {
                if (!is_dir($this->logDateDir)) {
                    mkdir(iconv('UTF-8', 'GBK', $this->logDateDir), 0777, true);
                }
            }
        } catch (Throwable $th) {
            if (strpos($th->getMessage(), 'mkdir') === false) {
                throw new CoralException($th->getMessage(), CoralException::Logger_ERROR_CODE);
            }
        }
    }

    /**
     * @throws CoralException
     */
    public function writeLog($log, $lev = self::INFO)
    {
        try {
            $this->appendLog($log, $lev);
            $this->write();
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::Logger_ERROR_CODE);
        }
    }

    /**
     * @throws CoralException
     */
    public function appendLog($log, $lev = self::INFO, bool $isShowTime = true)
    {
        try {
            $logString = '';
            if (!empty($lev) && $lev !== 0) {
                switch ($lev) {
                    case self::SUCCESS:
                        $logString = 'SUCCESS';
                        break;
                    case self::INFO:
                        $logString = 'INFO';
                        break;
                    case self::WARNING:
                        $logString = 'WARNING';
                        break;
                    case self::ERROR:
                        $logString = 'ERROR';
                        break;
                    case self::CRITICAL:
                        $logString = 'CRITICAL';
                        break;
                    case self::DEBUG:
                        $logString = 'DEBUG';
                        break;
                }
            }
            if (is_array($log)) {
                if ($this->isJsonPrint) {
                    $log = json_encode($log, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                } else {
                    $log = json_encode($log, JSON_UNESCAPED_UNICODE);
                }
            }
            $date      = date('Y-m-d H:i:s');
            $data      = $isShowTime ? "[{$date}] " : '';
            $logString = empty($logString) ? '' : "[{$logString}] ";
            if (empty($this->TraceID)) {
                $log = "{$data}{$logString} {$log}\n";
            } else {
                $log = "{$data}{$logString}[{$this->TraceID}] {$log}\n";
            }
            $this->log .= $log;
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::Logger_ERROR_CODE);
        }
    }

    /**
     * @throws CoralException
     */
    public function write(bool $parcel = true)
    {
        try {
            $this->filePathIsExist();
            if ($parcel) {
                $startLog = '--[' . date('Y-m-d H:i:s') . "]--start-----------------------------------------\n";
                $endLog   = "--end------------------------------------------------------------------\n";

                $this->log = $startLog . $this->log . $endLog;
            }

            $this->PutContents();
            if ($this->output) {
                echo($this->log);
            }
            $this->log = '';
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::Logger_ERROR_CODE);
        }
    }

    /**
     * @throws Exception
     */
    public function info($log)
    {
        $this->writeLine($log);
    }

    /**
     * @throws CoralException
     */
    public function writeLine($log, $lev = self::INFO)
    {
        try {
            $this->filePathIsExist();
            $this->appendLog($log, $lev);
            //  $logFileName = $this->logDateDir . DIRECTORY_SEPARATOR . $this->fileName;
            $this->PutContents();
            if ($this->output) {
                echo($this->log);
            }
            $this->log = '';
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::Logger_ERROR_CODE);
        }
    }

    /**
     * @throws Exception
     */
    public function success($log)
    {
        $this->writeLine($log, self::SUCCESS);
    }

    /**
     * @throws Exception
     */
    public function warning($log)
    {
        $this->writeLine($log, self::WARNING);
    }

    /**
     * @throws Exception
     */
    public function error($log)
    {
        $this->writeLine($log, self::ERROR);
    }

    /**
     * @throws Exception
     */
    public function critical($log)
    {
        $this->writeLine($log, self::CRITICAL);
    }

    /**
     * @throws Exception
     */
    public function debug($log)
    {
        $this->writeLine($log, self::DEBUG);
    }
}