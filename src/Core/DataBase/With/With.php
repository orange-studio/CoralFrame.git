<?php

namespace Coral\Core\DataBase\With;

use Coral\CoralException;
use Coral\Core\DataBase\BasePdo;
use Exception;
use Throwable;

class With
{
    private BasePdo $db;
    private string  $TableName      = '';
    private string  $FieldName      = '';
    private string  $JoinFieldsName = '';
    private array   $ChangeAlias    = [];
    private array   $Where          = [];

    public function __construct(BasePdo $db)
    {
        $this->db = $db;
    }

    /**
     * @throws Exception
     */
    public function Join(string $key, string $Join): With
    {
        try {
            $this->JoinFieldsName = $key;
            $JoinArr              = explode('.', $Join);
            if (empty($JoinArr[0])) {
                throw new Exception('tablename is required');
            }
            if (empty($JoinArr[1])) {
                throw new Exception('table field is required');
            }
            $this->TableName = $JoinArr[0];
            $this->FieldName = $JoinArr[1];
            return $this;
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::Pdo_Util_ERROR_CODE);
        }
    }

    public function Alias(array $alias): With
    {
        $this->ChangeAlias = $alias;
        return $this;
    }

    public function Where(array $where): With
    {
        $this->Where = array_merge($this->Where, $where);
        return $this;
    }

    /**
     * @throws Exception
     */
    public function Change(?array $data): array
    {
        if (empty($data)) {
            return [];
        }
        try {
            $isMoreArr = false;
            $ids       = [
                'id'   => [],
                'list' => []
            ];
            foreach ($data as $key => &$value) {
                if (is_array($value)) {
                    $isMoreArr = true;
                    if (!empty($value[$this->JoinFieldsName])) {
                        $ids['id'][] = $value[$this->JoinFieldsName];
                    }
                    foreach ($this->ChangeAlias as $vv) {
                        $value[$vv] = null;
                    }
                } else {
                    $isMoreArr = false;
                    if ($this->JoinFieldsName === $key) {
                        $ids['id'][] = $value;
                    }
                    foreach ($this->ChangeAlias as $vv) {
                        $data[$vv] = null;
                    }
                }
            }
            unset($value);
            if (empty($ids['id'])) {
                return $data;
            }
            $ids['id']   = array_unique($ids['id']);
            $where       = array_merge($this->Where, [$this->FieldName => $ids['id']]);
            $list        = $this->db->select($this->TableName, '*', $where);
            $ids['list'] = $list;
            if (empty($ids['list'])) {
                return $data;
            }
            foreach ($data as $key => &$value) {
                if (is_array($value) && !empty($value[$this->JoinFieldsName])) {
                    foreach ($ids['list'] as $k => $v) {
                        if ($v[$this->FieldName] == $value[$this->JoinFieldsName]) {
                            foreach ($this->ChangeAlias as $kk => $vv) {
                                $value[$vv] = $v[$kk] ?? '';
                            }
                        }
                    }
                } elseif ($this->JoinFieldsName === $key) {
                    foreach ($this->ChangeAlias as $kk => $vv) {
                        $data[$vv] = $ids['list'][0][$kk] ?? '';
                    }
                }
            }
            unset($value);
            if ($isMoreArr) {
                $data = array_values($data);
            }
            return $data;
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::Pdo_Util_ERROR_CODE);
        }
    }
}