<?php

namespace Coral\Core\Http\HttpBasic;

use Coral\Struct\HttpStatus;
use Coral\Tool\UtilTool;
use JsonException;
use Swoole\Http\Response as SwooleHttpResponse;

class HttpResponse
{
    private SwooleHttpResponse $response;
    private int                $Status     = 200;
    private array              $Cookies    = [];
    private array              $Headers    = [];
    private string             $ResponseContent;
    public bool                $IsWritable = false;

    public function __construct(SwooleHttpResponse $response)
    {
        $this->response = $response;
    }

    public function IsExit(): bool
    {
        return $this->IsWritable;
    }

    public function End()
    {
        $this->IsWritable = true;
    }

    public function GetResponse(): SwooleHttpResponse
    {
        return $this->response;
    }

    public function GetContent(): string
    {
        return $this->ResponseContent;
    }

    public function GetHeaders(): array
    {
        return $this->Headers;
    }

    public function GetCookies(): array
    {
        return $this->Cookies;
    }

    public function GetStatus(): int
    {
        return $this->Status;
    }

    /**
     * @throws JsonException
     */
    public function Write($content): bool
    {
        if (empty($content)) {
            return false;
        }
        if (is_array($content)) {
            $content = json_encode($content, JSON_THROW_ON_ERROR);
        }
        if (UtilTool::isJson($content)) {
            $this->SetHeader('Content-Type', 'application/json;charset=UTF-8');
        } else {
            $this->SetHeader('Content-Type', 'text/html;charset=UTF-8');
        }
        $res = $this->response->write($content);
        if ($res) {
            $this->IsWritable      = true;
            $this->ResponseContent = $content;
            $this->SetStatus(HttpStatus::CODE_OK);
        }
        return $res;
    }

    /**
     * @throws JsonException
     */
    public function WriteJson($content): bool
    {
        return $this->Write(json_encode($content, JSON_THROW_ON_ERROR));
    }

    public function SetHeader($key, $value, bool $format = false): bool
    {
        $res = $this->response->header($key, $value, $format);
        if ($res) {
            $this->Headers[$key] = $value;
        }
        return $res;
    }

    public function SetCookie(string $key, string $value = '', int $expire = 0, string $path = '/', string $domain = '', bool $secure = false, bool $httponly = true, string $samesite = '', string $priority = ''): bool
    {
        $res = $this->response->cookie($key, $value, $expire, $path, $domain, $secure, $httponly, $samesite, $priority);
        if ($res) {
            $this->Headers[$key] = $value;
        }
        return $res;
    }

    public function SetStatus(int $http_status_code): bool
    {
        $res = $this->response->status($http_status_code, HttpStatus::PHRASES[$http_status_code]);
        if ($res) {
            $this->Status = $http_status_code;
        }
        return $res;
    }

    public function Redirect(string $url, int $http_code = 302)
    {
        $this->response->redirect($url, $http_code);
    }

    public function Sendfile(string $filename, int $offset = 0, int $length = 0): bool
    {
        return $this->response->sendfile($filename, $offset, $length);
    }
}