<?php

namespace Coral\Core\Http\Middleware;

use Coral\Core\Http\Basic;
use Coral\Core\Http\HttpBasic\HttpRequest;
use Coral\Struct\RouteMiddle;
use Coral\Struct\RouteStruct;
use Coral\Tool\UtilTool;

class BaseMiddleware extends Basic
{
    public bool        $Next       = false;
    public array       $Middleware = [];
    public RouteMiddle $RouteMiddle;
    public RouteStruct $Route;
    public string      $class;
    public string      $action;

    public function __construct(HttpRequest $request, RouteStruct $Route, RouteMiddle $RouteMiddle)
    {
        $this->Route       = $Route;
        $this->Middleware  = $Route->Middleware;
        $this->RouteMiddle = $RouteMiddle;
        $this->class       = $Route->class;
        $this->action      = $Route->action;
        parent::__construct($request);
    }

    public function Next()
    {
        $NextMiddleware = UtilTool::findNextElement($this->Middleware, $this->RouteMiddle);
        if (!empty($NextMiddleware)) {
            $class  = $NextMiddleware->class;
            $action = $NextMiddleware->action;
            $Class  = new $class($this->request, $this->Route, $NextMiddleware);
        } else {
            $class  = $this->class;
            $action = $this->action;
            $Class  = new $class($this->request);
        }
        $Class->$action();
    }

}