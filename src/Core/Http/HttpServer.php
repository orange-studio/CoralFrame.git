<?php

namespace Coral\Core\Http;

use Coral\CoralException;
use Coral\Core\Http\HttpBasic\HttpRequest;
use Coral\Core\Route\RouteCollector;
use Coral\Instance\LOGInstance;
use Coral\Instance\RouterInstance;
use Coral\Struct\RouteMiddle;
use Coral\Struct\RouteStruct;
use Coral\Tool\ConfTool;
use Coral\Tool\FileDirTool;
use Coral\Tool\UtilTool;
use EasyTask\Table;
use Exception;
use Swoole\Http\Request;
use Swoole\Http\Response;
use Swoole\Http\Server;
use Throwable;

class HttpServer
{
    private int  $port      = 8077;
    private bool $daemonize = false;

    public function SetRoute(callable $callback)
    {
        $route = RouterInstance::getInstance();
        $callback($route);
    }

    /**
     * @param callable $callback 请求中的全局异常捕捉
     * @throws CoralException
     */
    public function Start(callable $callback)
    {
        try {
            $this->port = ConfTool::GetConfig('server.port');
            $http       = new Server('0.0.0.0', $this->port, SWOOLE_PROCESS);
            // 加载 httpserver 配置
            $this->LoadServerConfig($http);
            $http->on('WorkerStart', function ($server) {
                // $pid    = posix_getpid();
                // $logger = LOGInstance::getInstance()->SetInitFileConfig(ConfTool::GetConfig('Log.system_log'));
                // $logstr = $worker_id >= $server->setting['worker_num'] ? 'Task Worker' : 'Worker';
                // $logger->appendLog("{$logstr} PID={$pid}, Worker ID={$worker_id}", '', false);
                // $logger->write(false);
                \Swoole\Process::signal(SIGUSR1, function () use ($server) {
                    echo "Reloading...\n";
                    $server->reload(true);
                });
            });
            $http->on('start', function ($server) {

                $this->ConsoleRoute();
            });
            $http->on('Request', function (Request $request, Response $response) use ($callback) {
                $HttpReqesrt = new HttpRequest($request, $response);
                try {
                    ob_start();
                    $this->LoadRoute($HttpReqesrt);
                    $this->outConsole($HttpReqesrt);
                } catch (Throwable $th) {
                    $callback($HttpReqesrt, $th);
                } finally {
                    $output = ob_get_clean();
                    $logger = LOGInstance::getInstance()->SetTraceID($HttpReqesrt->GetTraceID())->SetInitFileConfig(ConfTool::GetConfig('Log.system_log'));
                    $logger->appendLog($output, '', false);
                    $logger->write(false);
                    unset($HttpReqesrt);
                    $response->end();
                }
            });
            $http->on('Shutdown', function ($server) {
                var_dump('Shutdown');
            });
            $http->start();
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::System_ERROR_CODE);
        }
    }

    /**
     * @throws CoralException
     */
    private function LoadRoute(HttpRequest $request)
    {
        try {
            $route = RouterInstance::getInstance();
            // 获取请求方法和URI
            $method    = $request->GetMethod();
            $uri       = $request->GetUri();
            $routeInfo = $route->Dispatch($request, $uri, $method);
            switch ($routeInfo[0]) {
                case RouteCollector::NOT_FOUND:
                    $request->GetResponse()->SetStatus(404);
                    $request->GetResponse()->Write('Not found');
                    break;
                case RouteCollector::METHOD_NOT_ALLOWED:
                    $request->GetResponse()->SetStatus(405);
                    $request->GetResponse()->Write('Method not allowed');
                    break;
                case RouteCollector::FOUND:
                    // 执行路由处理程序
                    if ($routeInfo[1] instanceof RouteStruct) {
                        $this->Handle($request, $routeInfo[1]);
                    }
                    break;
            }
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::Router_ERROR_CODE);
        }
    }

    /**
     * @throws CoralException
     */
    private function Handle(HttpRequest $request, RouteStruct $route): void
    {
        try {
            if (!empty($route->Middleware) && count($route->Middleware) > 0) {
                $FirstMiddleware = UtilTool::findNextElement($route->Middleware);
                $class           = $FirstMiddleware->class;
                $action          = $FirstMiddleware->action;
                if (!empty($class) && !empty($action)) {
                    $Class = new $class($request, $route, $FirstMiddleware);
                    $Class->$action(function () use ($request, $route) {
                        $Class  = $route->class;
                        $action = $route->action;
                        $Class  = new $Class($request);
                        $Class->$action();
                    });
                }
            } else {
                $Class  = $route->class;
                $action = $route->action;
                $Class  = new $Class($request);
                $Class->$action();
            }
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::System_ERROR_CODE);
        }
    }

    /**
     * @throws CoralException
     */
    private function ConsoleRoute()
    {
        try {
            $route   = RouterInstance::getInstance()->Routes;
            $headers = ['ADDRESS', 'METHOD', 'ROUTE', 'HANDLER', 'MIDDLEWARE'];
            $data    = [];
            $length  = [];
            foreach ($route as $v) {
                if ($v instanceof RouteStruct) {
                    $MiddlewareArr = [];
                    foreach ($v->Middleware as $RouteMiddle) {
                        if ($RouteMiddle instanceof RouteMiddle) {
                            if (!empty($RouteMiddle->class) && !empty($RouteMiddle->action)) {
                                //                                $MiddlewareArr[] = implode('::', [$RouteMiddle->class, $RouteMiddle->action]);
                                $MiddlewareArr[] = $RouteMiddle->action;
                            } else {
                                $MiddlewareArr[] = 'NULL';
                            }
                        }
                    }
                    $item   = [
                        $this->port,
                        $v->method,
                        $v->path,
                        $v->class . '::' . $v->action,
                        implode(',', $MiddlewareArr),
                    ];
                    $length = [
                        min(20, max(10, $length[0] ?? 0, strlen($item[0]))),
                        min(20, max(10, $length[1] ?? 0, strlen($item[1]))),
                        min(60, max(10, $length[2] ?? 0, strlen($item[2]))),
                        min(100, max(10, $length[3] ?? 0, strlen($item[3]))),
                        min(100, max(20, $length[4] ?? 0, strlen($item[4]))),
                    ];
                    $data[] = $item;
                }

            }
            $table = new table();
            $table->setHeader($headers);
            $table->setStyle('box');
            $printTextArr = $table->render($data);
            $logger       = LOGInstance::getInstance()->SetInitFileConfig(ConfTool::GetConfig('Log.system_log'));
            $logger->appendLog("\n$printTextArr", '', false);
            $logger->write(false);
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::System_ERROR_CODE);
        }
    }

    /**
     * @Description 请求记录输出控制台
     * @throws CoralException
     */
    private function outConsole(HttpRequest $request): void
    {
        try {
            $method      = $request->getMethod();
            $statusCode  = $request->GetResponse()->GetStatus();
            $url         = $request->GetAllUri();
            $TraceID     = $request->GetTraceID();
            $RequestInfo = "{$statusCode} [{$method}] {$url}\n";
            $logger      = LOGInstance::getInstance()->SetInitFileConfig(ConfTool::GetConfig('Log.system_log'))->SetTraceID($TraceID);
            $logger->appendLog($RequestInfo);
            $logger->write(false);
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::System_ERROR_CODE);
        }
    }

    /**
     * @throws Exception
     */
    private function LoadServerConfig(Server $http)
    {
        try {
            $httpConfig = ConfTool::GetConfig('request');
            foreach ($httpConfig as $key => &$value) {
                if ($key == 'log_file') {
                    $value = UtilTool::NormalizedPath(CORAL_ROOT . DIRECTORY_SEPARATOR . $value);
                    FileDirTool::touchFile($value);
                }
                if ($key == 'pid_file') {
                    $value = UtilTool::NormalizedPath(CORAL_ROOT . DIRECTORY_SEPARATOR . $value);
                    FileDirTool::touchFile($value);
                }
            }
            unset($value);
            $http->set($httpConfig);
        } catch (Throwable $th) {
            throw new Exception($th->getMessage(), CoralException::System_ERROR_CODE);
        }
    }
}