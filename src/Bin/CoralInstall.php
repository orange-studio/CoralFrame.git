<?php
define('CORAL_ROOT', getcwd());

const CORAL_CONFIG_ROOT   = CORAL_ROOT . DIRECTORY_SEPARATOR . 'config.yaml';
const CORAL_COMPOSER_ROOT = CORAL_ROOT . DIRECTORY_SEPARATOR . 'vendor/coralframe/studio/src';

require_once CORAL_ROOT . DIRECTORY_SEPARATOR . 'vendor/autoload.php';


(new \Coral\Install())->InitDir();