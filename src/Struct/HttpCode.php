<?php

namespace Coral\Struct;

class HttpCode
{
    public int    $Code    = 0;
    public string $Message = 'SUCCESS';
    public ?array $Data    = null;
}