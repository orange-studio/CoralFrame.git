<?php

namespace Coral\Extend;

use Coral\Core\Logger;
use Exception;

/**
 * 日志组件
 */
class LOG extends Logger
{
    /**
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();
    }
}