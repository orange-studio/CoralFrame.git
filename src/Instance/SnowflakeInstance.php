<?php

namespace Coral\Instance;

use Coral\CoralException;
use Coral\Tool\ConfTool;
use Godruoyi\Snowflake\Snowflake;
use Throwable;

class SnowflakeInstance
{
    protected static ?Snowflake $_instance = null;

    /**
     * @throws CoralException
     */
    public static function getInstance(): Snowflake
    {
        try {
            if (self::$_instance === null) {
                $dataCenterId    = ConfTool::GetConfig('Uuid.DataCenterId');
                $workerId        = ConfTool::GetConfig('Uuid.Workerid');
                self::$_instance = new Snowflake($dataCenterId, $workerId);
            }
            return self::$_instance;
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), $th->getCode());
        }
    }
}