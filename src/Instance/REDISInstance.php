<?php

namespace Coral\Instance;

use Coral\CoralException;
use Coral\Extend\REDIS;
use Throwable;

class REDISInstance
{
    protected static ?REDIS $_instance = null;

    /**
     * @throws CoralException
     */
    public static function getInstance(string $configName = 'default'): REDIS
    {
        try {
            if (self::$_instance === null) {
                self::$_instance = new REDIS($configName);
            }
            return self::$_instance;
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), $th->getCode());
        }
    }
}