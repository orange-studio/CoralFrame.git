<?php

namespace Coral\Instance;

use Coral\CoralException;
use Coral\Extend\LOG;
use Throwable;

class LOGInstance
{
    protected static ?LOG $_instance = null;

    /**
     * @throws CoralException
     */
    public static function getInstance(): LOG
    {
        try {
            if (self::$_instance === null) {
                self::$_instance = new LOG();
            }
            return self::$_instance;
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), $th->getCode());
        }
    }
}