<?php

namespace Coral\Tool;

use Coral\CoralException;
use Coral\Core\Http\HttpBasic\HttpRequest;
use Coral\Instance\SnowflakeInstance;
use Throwable;
use voku\helper\AntiXSS;

class UtilTool
{

    /**
     * 生产一个UUID4
     * 有概率重复|短时间内可以认为唯一
     *
     * @return string
     * @throws CoralException
     */
    public static function makeUUIDV4(): string
    {
        try {
            mt_srand();
            $charId = strtolower(md5(uniqid(mt_rand(), true)));
            $hyphen = '-';

            return substr($charId, 0, 8) . $hyphen .
                substr($charId, 8, 4) . $hyphen .
                substr($charId, 12, 4) . $hyphen .
                substr($charId, 16, 4) . $hyphen .
                substr($charId, 20, 12);
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::UtilTool_ERROR_CODE);
        }
    }

    /**
     * 雪花唯一ID
     *
     * @param string $prefix
     * @param int $dataCenterId
     * @param int $workerId
     *
     * @return string
     * @throws CoralException
     */
    public static function makeSnowFlake(string $prefix = '', int $dataCenterId = 0, int $workerId = 0): string
    {
        try {
            $sf = SnowflakeInstance::getInstance($dataCenterId, $workerId);
            $sf = $sf->setStartTimeStamp(time());
            $id = $sf->id();

            return $prefix . $id;
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::UtilTool_ERROR_CODE);
        }
    }

    /**
     * 生成订单号 不设置前缀为22位订单号
     *
     * @param string $prefix
     * @param int $dataCenterId
     * @param int $workerId
     *
     * @return string
     * @throws CoralException
     */
    public static function makeOrderSn(string $prefix = '', int $dataCenterId = 0, int $workerId = 0): string
    {
        try {
            $sf = SnowflakeInstance::getInstance($dataCenterId, $workerId);
            $sf->setStartTimeStamp(time());
            $str = $sf->id();

            return $prefix . date('ymdHis') . substr($str, 12, 7) . self::character(3, '0123456789');
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::UtilTool_ERROR_CODE);
        }
    }

    /**
     * 生产随机数
     *
     * @param int $length 长度
     * @param string $alphabet 元素集合
     *
     * @return false|string
     * @throws CoralException
     */
    public static function character(int $length = 6, string $alphabet = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789'): string
    {
        try {
            mt_srand();
            if ($length >= strlen($alphabet)) {
                $rate     = (int)($length / strlen($alphabet)) + 1;
                $alphabet = str_repeat($alphabet, $rate);
            }

            return substr(str_shuffle($alphabet), 0, $length);
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::UtilTool_ERROR_CODE);
        }
    }


    /**
     * XSS过滤
     *
     * @param string $value
     * @param false $isClean
     *
     * @return bool|mixed|string|string[]|null
     */
    public static function checkXss(string $value, bool $isClean = false)
    {
        $antiXss         = new AntiXSS();
        $harmless_string = $antiXss->xss_clean($value);
        if ($isClean) {
            return $harmless_string;
        }

        return $antiXss->isXssFound();
    }

    /**
     * 父子关系的扁平数据列表转换为树形结构
     *
     * @param array $list 数据源
     * @param string $pk 数据 ID
     * @param string $pid 父级数据 ID
     * @param string $child 子数据 key
     * @param int $root 最大父级ID
     * @return array
     */
    public static function listToTree(array $list, string $pk = 'id', string $pid = 'pid', string $child = 'child', int $root = 0): array
    {
        $tree     = [];
        $packData = [];
        foreach ($list as $data) {
            $packData[$data[$pk]]         = $data;
            $packData[$data[$pk]][$child] = [];  // 初始化为空数组
        }
        foreach ($packData as $key => &$val) {
            if ($val[$pid] == $root) {
                // 代表根节点
                $tree[] =& $val;
            } else {
                // 找到其父类
                $packData[$val[$pid]][$child][] =& $packData[$key];
            }
        }
        return $tree;
    }

    public static function getClientIp(HttpRequest $request)
    {
        $realIps = $request->GetHeader('X-Forwarded-For');
        if (!empty($realIps)) {
            return explode(',', $realIps)[0];
        }
        $ip = $request->GetHeader('Proxy-Client-IP');
        if (empty($ip)) {
            $ip = $request->GetHeader('WL-Proxy-Client-IP');
        }
        if (empty($ip)) {
            $ip = $request->GetHeader('HTTP_CLIENT_IP');
        }
        if (empty($ip)) {
            $ip = $request->GetHeader('HTTP_X_FORWARDED_FOR');
        }
        if (empty($ip)) {
            $ip = $request->GetHeader('X-Real-IP');
        }
        if (empty($ip)) {
            $ip = $request->GetServer()['remote_addr'];
        }
        return $ip;
    }

    public static function isJson(string $string): bool
    {
        if (empty($string)) {
            return false;
        }
        json_decode($string);
        return json_last_error() === JSON_ERROR_NONE;
    }

    public static function findNextElement($array, $currentElement = null)
    {
        $found = false; // 标记是否找到目标元素
        foreach ($array as $element) {
            if ($found || empty($currentElement)) {
                return $element;
            }
            if ($element == $currentElement) {
                $found = true;
            }
        }
        return null;
    }

    public static function NormalizedPath(string $path)
    {
        return preg_replace('#/+#', '/', $path);
    }
}