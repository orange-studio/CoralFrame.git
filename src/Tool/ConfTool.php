<?php

namespace Coral\Tool;

use Coral\CoralException;
use Symfony\Component\Yaml\Yaml;

class ConfTool
{
    /**
     * 获取 YAML 配置文件内容
     *
     * @return mixed
     */
    public static function GetConfigs()
    {
        return Yaml::parseFile(CORAL_CONFIG_ROOT);
    }

    /**
     * @throws CoralException
     */
    public static function GetConfig(string $key)
    {
        $KeyArr = explode('.', $key);
        $Config = self::GetConfigs();
        foreach ($KeyArr as $key) {
            if (empty($key)) {
                continue;
            }
            if (empty($Config[$key]) && $Config[$key] !== 0 && $Config[$key] !== false) {
                throw new CoralException("The configuration {$key} does not exist");
            }
            $Config = $Config[$key];
        }
        return $Config;
    }
}