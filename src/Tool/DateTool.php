<?php

namespace Coral\Tool;

use Coral\CoralException;
use Throwable;

class DateTool
{

    /**
     * 获取当前毫秒时间戳
     *
     * @return float
     */
    public static function getMsectime(): float
    {
        [$msec, $sec] = explode(' ', microtime());

        return bcmul($msec + $sec, 1000);
    }

    /**
     * 获取当前微秒
     *
     * @throws CoralException
     */
    public static function getMicroTime(): string
    {
        try {
            [$usc, $sec] = explode(' ', microtime());

            return ($sec . substr($usc, 2, 3));
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::UtilTool_ERROR_CODE);
        }
    }

    /**
     * 获取当日零点时间
     *
     * @throws CoralException
     */
    public static function getTodayZero()
    {
        try {
            return strtotime(date('Y-m-d 00:00:00', time()));
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::UtilTool_ERROR_CODE);
        }
    }

    /**
     * 获取本周所有日期
     *
     * @param int $time 时间戳
     * @param string $format 格式
     *
     * @return array
     * @throws CoralException
     */
    public static function weekList(int $time = 0, string $format = 'Y-m-d'): array
    {
        try {
            $time = $time > 0 ? $time : time();
            $week = date('w', $time);
            $date = [];
            for ($i = 1; $i <= 7; $i++) {
                $date[$i] = date($format, strtotime('+' . ($i - $week) . ' days', $time));
            }

            return $date;
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::UtilTool_ERROR_CODE);
        }
    }

    /**
     * 时间显示函数
     *
     * @param int $unixTime $unixTime  or string $unixTime 时间戳或者时间字符串
     * @param int $limit 相差时间间隔
     * @param string $format 超出时间间隔的日期显示格式
     *
     * @return string 返回需要的时间格式
     * @throws CoralException
     */
    public static function showTime(int $unixTime, int $limit = 18000, string $format = 'Y-m-d'): string
    {
        try {
            $nowTime = time();
            if (!is_int($unixTime)) {
                $unixTime = strtotime($unixTime);
            }
            $differ = $nowTime - $unixTime;
            if ($differ >= 0) {
                if ($differ > $limit) {
                    $showtime = date($format, $unixTime);
                } else {
                    $showtime = $differ > 86400 ? floor($differ / 86400) . '天前' : ($differ > 3600 ? floor($differ / 3600) . '小时前' : floor($differ / 60) . '分钟前');
                }
            } else {
                if (-$differ > $limit) {
                    $showtime = date($format, $unixTime);
                } else {
                    $showtime = -$differ > 86400 ? floor(-$differ / 86400) . '天' : (-$differ > 3600 ? floor(-$differ / 3600) . '小时' : floor(-$differ / 60) . '分钟');
                }
            }

            return $showtime;
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::UtilTool_ERROR_CODE);
        }
    }

    /**
     * 获取当前时间和参数时间相差的天数
     *
     * @param int $timestamp
     *
     * @return float|int
     * @throws CoralException
     */
    public static function diffDayNum(int $timestamp)
    {
        try {
            $nowDay = date('Y-m-d');
            $sysDay = date('Y-m-d', $timestamp);
            $day    = strtotime($nowDay) - strtotime($sysDay);

            return $day / 86400;
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::UtilTool_ERROR_CODE);
        }
    }

    /**
     * 时间差计算
     *
     * @param int $timestamp
     *
     * @return string
     * @throws CoralException
     */
    public static function roundTime(int $timestamp): string
    {
        try {
            $now  = time();
            $time = $timestamp - $now;
            if ($time > 0) {
                $suffix = '之后';
            } else {
                $suffix = '之前';
            }
            $fixTime = $roundTime = '';
            $time    = abs($time);
            if ($time < 60) {
                $fixTime   = '秒';
                $roundTime = $time;
            } elseif ($time < 3600) {
                $fixTime   = '分钟';
                $roundTime = round($time / 60);
            } elseif ($time < 3600 * 24) {
                $fixTime   = '小时';
                $roundTime = round($time / 3600);
            } elseif ($time < 3600 * 24 * 7) {
                $fixTime   = '天';
                $roundTime = round($time / (3600 * 24));
            } elseif ($time < 3600 * 24 * 30) {
                $fixTime   = '周';
                $roundTime = round($time / (3600 * 24 * 7));
            } elseif ($time < 3600 * 24 * 365) {
                $fixTime   = '个月';
                $roundTime = round($time / (3600 * 24 * 30));
            } elseif ($time >= 3600 * 24 * 365) {
                $fixTime   = '年';
                $roundTime = round($time / (3600 * 24 * 365));
            }

            return $roundTime . ' ' . $fixTime . $suffix;
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::UtilTool_ERROR_CODE);
        }
    }

    /**
     * 获取某天的开始结束时间
     *
     * @param string $date
     *
     * @return array
     * @throws CoralException
     */
    public static function startAndEndTimestamp(string $date = ''): array
    {
        try {
            $date      = empty($date) ? date('Y-m-d') : $date;
            $startTime = strtotime($date);
            $endTime   = $startTime + 60 * 60 * 24;

            return [$startTime, $endTime];
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::UtilTool_ERROR_CODE);
        }
    }

    /**
     * @description 获取当前日期的【一周，一个月】日期列表
     *
     * @param string $type 类型
     * @param int $time 时间
     * @param string $format 格式
     *
     * @return array
     * @throws CoralException
     */
    public static function getDateList(string $type = 'W', int $time = 0, string $format = 'Y-m-d'): array
    {
        try {
            $time = $time > 0 ? $time : time();
            //获取当前周几
            $number  = $type === 'W' ? date('w', $time) : date('d', $time);
            $date    = [];
            $allDays = $type === 'W' ? 7 : date('t', $time);
            for ($i = 1; $i <= $allDays; $i++) {
                $date[] = date($format, strtotime('+' . ($i - $number) . ' days', $time));
            }
            return $date;
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::UtilTool_ERROR_CODE);
        }
    }

}